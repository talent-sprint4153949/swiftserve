package com.utility;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;


@Component
public class EmailUtility {
	
	@Autowired
	private JavaMailSender javaMailSender;
	
	public void sendOtpEmail(String email,String name,String otp)throws MessagingException{
		MimeMessage mimeMessage = javaMailSender.createMimeMessage();
		MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage);
		mimeMessageHelper.setTo(email);
		mimeMessageHelper.setSubject("OTP for Reset the password");
		mimeMessageHelper.setText("Dear"+name+""
				+ "Please use the below One Time Password"+
				"One Time Password(OTp):"+otp);
		javaMailSender.send(mimeMessage);
	}

}

package com.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Company {

	@Id@GeneratedValue
	private int id;
	private String email;
	private String password;
	private String name;
	private String number;
	@Column(columnDefinition = "varchar(225) default 'inactive'",insertable=false)
	private String status  ;
	
	@OneToMany(mappedBy="company")
	List<Services> services=new ArrayList<Services>();
	

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNumber(){
		return number;
	}
	public void setNumber(String number){
		this.number=number;
	}
	
	public String getStatus(){
		return status;
	}
	
	public Company(int id, String email, String password, String name, String number, String status,
			List<Services> services) {
		super();
		this.id = id;
		this.email = email;
		this.password = password;
		this.name = name;
		this.number = number;
		this.status = "inactive";
		this.services = services;
	}
	public Company() {
		super();
		// TODO Auto-generated constructor stub
	}
	public void setStatus(String status){
		this.status="inactive";
	}

	
	
}

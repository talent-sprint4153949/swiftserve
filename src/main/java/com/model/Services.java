
package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Services {
	@Id
    @GeneratedValue
    private int servicesId;

    private String companyName;
    private String email;
    private String imageUrl;
    private String serviceType;
    private String description;
 
    private double servicePrice;

    @ManyToOne
    @JoinColumn(name = "id")
    private Company company;

    public Services() {
        // Default constructor
    }

    public Services(String companyName,String email, String imageUrl, String serviceType, String description, double servicePrice, Company company) {
        super();
        this.companyName = companyName;
        this.email=email;
        this.imageUrl = imageUrl;
        this.serviceType = serviceType;
        this.description = description;
        
        this.servicePrice = servicePrice;
        this.company = company;
    }

    public int getServicesId() {
        return servicesId;
    }

    public void setServicesId(int servicesId) {
        this.servicesId = servicesId;
    }

   
    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
    public String getEmail(){
    	return email;
    }
    
    public void setEmail(String email){
    	this.email=email;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

   
    public double getServicePrice() {
        return servicePrice;
    }

    public void setServicePrice(double servicePrice) {
        this.servicePrice = servicePrice;
    }
    public Company getCompany() {
        return this.company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

	
	

}

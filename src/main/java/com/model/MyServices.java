package com.model;

public class MyServices {

	
	private int id;
	private boolean status;
	private String name;
	public MyServices(int id, boolean status ,String name) {
		super();
		this.id = id;
		this.status = status;
		this.name=name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public MyServices() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public boolean getStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	
}

package com.model;

public class MyCustomer {

    private int custId;
	private boolean status;
	private String firstName;
	
	
	public MyCustomer(int custId, boolean status,String firstName ){
		super();
		this.custId=custId;
		this.status=status;
		this.firstName=firstName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public MyCustomer(){
		super();
	}
	public int getCustId() {
		return custId;
	}
	public void setCustId(int custId) {
		this.custId = custId;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	
	  	
}

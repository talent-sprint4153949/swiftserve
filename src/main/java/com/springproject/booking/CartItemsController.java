package com.springproject.booking;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.CartItemsDao;
import com.model.CartItems;

@RestController
public class CartItemsController {

    @Autowired
    CartItemsDao cartItemsDAO;

    @PostMapping("/regCartbyCustId/{custId}")
    @CrossOrigin
    public String regCartbyCustId(@RequestBody CartItems cartItems, @PathVariable int custId) {
        cartItemsDAO.regCartbyCustId(cartItems, custId);
        return "Added Successfully";
    }

    @GetMapping("/findCartBycustId/{custId}")
    @CrossOrigin
    public List<CartItems> findCartBycustId(@PathVariable int custId) {
        return cartItemsDAO.findCartBycustId(custId);
    }
    
    @DeleteMapping("/delCartItemsbyid/{cartId}")
    @CrossOrigin
    
    public String delCartItemsbyid(@PathVariable int cartId){
    	cartItemsDAO.delCartItemsbyid(cartId);
    	return "Deleted Succesfully";
    }
}

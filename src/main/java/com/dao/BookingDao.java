package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.stereotype.Service;

import com.model.Booking;
import com.model.Customer;
import com.model.Services;
import com.utility.EmailUtility;
import com.utility.Otputil;


@Service
public class BookingDao {

	
	@Autowired
    private BookingRepo bookingRepo;
	
	@Autowired
	private CustomerRepo customerRepo;
	
//	@Autowired
//	private Services services;
//	
//	@Autowired
//	private EmailUtility emailUtility;
//	
//	@Autowired
//	private Otputil otpUtil;
	
	
	public String getRegBooking(Booking booking){
		Customer customer = customerRepo.findById(booking.getCustomer().getCustId()).orElse(null);
		if(customer!=null){
			booking.setCustomer(customer);
			bookingRepo.save(booking);
			return "reg Success";
		}
		return "not Found";
		
	}
	
	public List<Booking> getAllBookings(){
		return bookingRepo.findAll();
	}
	
	public List<Booking> findBookingByCustId(int custId){
		return bookingRepo.findByCustomer_CustId(custId);
	}
	
	public String regBookbyCustId(Booking booking,int custId){
		Customer customer =new Customer();
		customer.setCustId(custId);
		booking.setCustomer(customer);
		bookingRepo.save(booking);
		
		return "Book Services Registered Succesfully";
		
	}
	
	
//	public String getRegisterBooking(Booking booking){
//		Customer customer = customerRepo.findById(booking.getCustomer().getCustId());
//		if(customer !=null){
//			booking.setCustomer(customer);
//			bookingRepo.save(booking);
//		}
//		
//		Service service = services.getServiceType();
//		String serviceEmail = services.getEmail();
//		if(serviceEmail !=null && !serviceEmail.isEmpty()){
//			String subject = "A new Booking Registered";
//			String message = "A New Registered Booking has came"+ services.getServiceType();
//			try{
//				emailUtility.sendOtpEmail(serviceEmail, services.getCompanyName(), subject);
//			}catch( NotFoundException e){
//				e.printStackTrace();
//			}
//		}
//		return "reg Sucess";
//		
//		
//	}

	
	
	
	
}

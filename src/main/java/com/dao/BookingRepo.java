package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.model.Booking;

@Repository
public interface BookingRepo extends JpaRepository<Booking,Integer> {
    List<Booking> findByCustomer_CustId(int custId);
}

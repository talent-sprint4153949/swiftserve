package com.dao;

import java.util.List;

import javax.mail.MessagingException;
import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import com.model.Customer;

import com.model.MyServices;
import com.utility.EmailUtility;
import com.utility.Otputil;


@Service
@Transactional
public class CustomerDao {

    @Autowired
    CustomerRepo customerRepo;
    
    @Autowired
    private Otputil otpUtil;
    
    @Autowired
    private EmailUtility emailutil;
    
    PasswordEncoder passwordEncoder;
    
    public CustomerDao (CustomerRepo customerRepo){
    	this.passwordEncoder=new BCryptPasswordEncoder();
    }

    public String getRegCustomer(Customer customer){
    	customer.setPassword(this.passwordEncoder.encode(customer.getPassword()));
        customerRepo.save(customer);
        return "Customer Registered Successfully";
    }
    
    public List<Customer> getAllCustomer(){
        return customerRepo.findAll();
    }
    
  
    public MyServices validateCustomer(String email, String password) {
        Customer customer = findByEmail(email);
        if (customer != null && passwordEncoder.matches(password, customer.getPassword())) {
            return new MyServices(customer.getCustId(), true,customer.getFirstName());
        } else {
            return new MyServices(0, false,"");
        }
    }


	public Customer findByEmail(String email) {
		return customerRepo.findByEmail(email);
	}
    
	
	public String sendOtpEmail(String email){
		Customer customer = customerRepo.findByEmail(email);
		String otp=otpUtil.generateOtp();
		if(customer!=null){
		
			try {
				emailutil.sendOtpEmail(customer.getEmail(),customer.getFirstName(), otp);
			} catch (MessagingException e) {
				throw new RuntimeException("Unable To Send Otp");
			}
		}else{
			return "User Not Found";
		}
		return otp;
	}
	
//	public Customer updateCustomerPassword(Customer customer){
//		
//		Customer existCustomer =customerRepo.findByEmail(customer.getEmail());
//		if(existCustomer ==null){
//			return  new Customer();
//		}
//		
//	BCryptPasswordEncoder  passwordEncoder = new BCryptPasswordEncoder();
//	 String encryptedpassword=passwordEncoder.encode(customer.getPassword());
//	 existCustomer.setPassword(encryptedpassword);
//	 return customerRepo.save(existCustomer);
//	
//	
//	}
	
	
	public Customer updateCustomerPassword(Customer customer){
		Customer cust = customerRepo.findByEmail(customer.getEmail());
		if(cust ==null){
			return new Customer();
		}
		
		String encryptPassword = passwordEncoder.encode(customer.getPassword());
		cust.setPassword(encryptPassword);
		return customerRepo.save(cust);
	}
	
	
	

	
    
    
    
    
   
}


package com.dao;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Company;

@Repository
public interface CompanyRepo extends JpaRepository <Company,Integer> {
	
	Company findByEmail(String email);

	@Modifying
	@Query("UPDATE Company c SET c.status = 'active' WHERE c.id = :id")
	void updateStatusById(@Param("id") int id);

	
	

}
